import os
from flask import Flask

def create_app(test_config=None):
        # Create app
        app = Flask(__name__, instance_relative_config=True)
        app.config.from_mapping(
                SECRET_KEY='dev',
                DATABASE=os.path.join(app.instance_path, 'micro-ci.sqlite')
        )

        if test_config is None:
            # Load the instance config
            app.config.from_pyfile('config.py', silent=True)
        else:
            # Load the test config if passed in
            app.config.from_mapping(test_config)

        try:
            os.makedirs(app.instance_path)
        except OSError:
            pass

        from . import db
        db.init_app(app)

        from . import auth
        app.register_blueprint(auth.bp)

        from . import dashboard
        app.register_blueprint(dashboard.bp)
        app.add_url_rule('/', endpoint='index')

        from . import application
        app.register_blueprint(application.bp)

        # test route
        @app.route('/hello')
        def hello():
            return 'Oh, hi there!'

        return app
