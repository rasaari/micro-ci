from flask import (
    Blueprint, flash, g, redirect, render_template, request, url_for
)

from werkzeug.exceptions import abort

from microci.auth import login_required
from microci.db import get_db

bp = Blueprint('dashboard', __name__)

@bp.route('/')
@login_required
def index():
    db = get_db()

    apps = db.execute('SELECT * FROM app').fetchall()

    return render_template('dashboard/index.html', apps=apps)
