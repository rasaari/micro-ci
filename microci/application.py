from flask import (
    Blueprint, flash, g, redirect, render_template, request, url_for
)

from werkzeug.exceptions import abort

from microci.auth import login_required
from microci.db import get_db

bp = Blueprint('app', __name__)

@bp.route('/')
def index():
    #db = get_db()

    #apps = db.execute('SELECT * FROM app').fetchall()

    #return render_template('dashboard/index.html', apps=apps)
    return "Oh, hi!"

@bp.route('/create', methods=('GET', 'POST'))
@login_required
def create():
    if request.method == 'POST':
        name = request.form['name']

    return render_template('app/create.html')
